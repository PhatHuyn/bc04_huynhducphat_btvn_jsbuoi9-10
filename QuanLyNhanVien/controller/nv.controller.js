function LayThongTintuForm(){
    const taikhoan = document.getElementById("tknv").value;
    const hoten = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const matkhau = document.getElementById("password").value;
    const ngaylam = document.getElementById("datepicker").value;
    const luongcoban = document.getElementById("luongCB").value;
    const chucvu = document.getElementById("chucvu").value;
    const giolam = document.getElementById("gioLam").value;

    return new NhanVien(taikhoan, hoten, email, matkhau, ngaylam, luongcoban, chucvu, giolam);
}
function LayThongTintuForm2(){
    const taikhoan = document.getElementById("tknv2").value;
    const hoten = document.getElementById("name2").value;
    const email = document.getElementById("email2").value;
    const matkhau = document.getElementById("password2").value;
    const ngaylam = document.getElementById("datepicker2").value;
    const luongcoban = document.getElementById("luongCB2").value;
    const chucvu = document.getElementById("chucvu2").value;
    const giolam = document.getElementById("gioLam2").value;

    return new NhanVien(taikhoan, hoten, email, matkhau, ngaylam, luongcoban, chucvu, giolam);
}

//render danh sách nhân viên ra giao diện
function renderDSNV(nvArray){ 
    var contentHTML = "";
    for(var i = 0; i< nvArray.length; i++){
        var nv = nvArray[i];
        //tr content trong mỗi lần lập
        var trConTent = 
        `<tr>
            <td>${nv.TaiKhoanNV}</td>
            <td>${nv.HoTenNV}</td>
            <td>${nv.EmailNV}</td>
            <td>${nv.NgayLamNV}</td>
            <td>${nv.ChucVuNV}</td>
            <td>${new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(nv.TinhTongLuong())}</td>
            <td>${nv.XepLoaiNV()}</td>
            <td>
            <button onclick="suaNhanVien('${nv.TaiKhoanNV}')" class="btn btn-warning" data-toggle="modal" data-target="#myModal2">Sửa</button>
            <button onclick="xoaNhanVien('${nv.TaiKhoanNV}')" class="btn btn-danger">Xoá</button>
            </td>
        </tr> `;
        contentHTML += trConTent;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}   

function TimKiemViTri(id, dsnv){
    for(var i = 0; i<dsnv.length;i++){
        var nv = dsnv[i];
        if(nv.TaiKhoanNV == id){
            return i;
        }
    }
    return -1;
}

function ShowThongTinLenForm(nv){
    document.getElementById("tknv2").value = nv.TaiKhoanNV;
    document.getElementById("name2").value = nv.HoTenNV;
    document.getElementById("email2").value = nv.EmailNV;
    document.getElementById("password2").value  = nv.MatKhauNV;
    document.getElementById("datepicker2").value = nv.NgayLamNV;
    document.getElementById("luongCB2").value = nv.LuongCoBanNV;
    document.getElementById("chucvu2").value = nv.ChucVuNV;
    document.getElementById("gioLam2").value = nv.GioLamNV;
}

function CapNhatnv(nvCapNhat, dsnv){
    console.log('nvCapNhat.TaiKhoanNV: ', nvCapNhat.TaiKhoanNV);

    for(var i = 0; i<dsnv.length; i++){
        var nvUpdate = dsnv[i];
        console.log('nvUpdate: ', nvUpdate);
        if(nvCapNhat.TaiKhoanNV == nvUpdate.TaiKhoanNV){
            nvUpdate.HoTenNV = nvCapNhat.HoTenNV;
            nvUpdate.EmailNV = nvCapNhat.EmailNV;
            nvUpdate.MatKhauNV = nvCapNhat.MatKhauNV;
            nvUpdate.NgayLamNV = nvCapNhat.NgayLamNV;
            nvUpdate.LuongCoBanNV = nvCapNhat.LuongCoBanNV;
            nvUpdate.ChucVuNV = nvCapNhat.ChucVuNV;
            nvUpdate.GioLamNV = nvCapNhat.GioLamNV;
        }
    }
    return dsnv;
}