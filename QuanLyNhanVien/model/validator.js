var validate = {
    kiemTraRong: function(value, idError, message){
        if(value.length == 0){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemTraDoDai: function(value, idError, message, min, max){
        if(value.length < min || value.length > max){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemtraEmail: function(value, idError, message){
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if( re.test(value)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    kiemtraChu: function(value, idError, message){
        
        // var lt = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u;
        var letters = /^[A-Za-z]+$/;
        if(letters.test(value))
        {
            document.getElementById(idError).innerText = "";
            return true;
        }
        else
        {
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    kiemtraSo: function(value, idError, message){
        var regex=/^[0-9]+$/;        
        if(regex.test(value))
        {
            document.getElementById(idError).innerText = "";
            return true;
        }
        else
        {
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    kiemtraGio: function(value, idError, message){
        if(value < 80 || value > 200){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    kiemtraLuong: function(value, idError, message){
        if(value < 1000000 || value > 20000000){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    // kiemtraDate: function(value, idError, message){
    //     const regex = /^\d{2}\/\d{2}\/\d{4}$/;
  
    //     if (value.match(regex) === null) {
    //     return false;
    //     }
    
    //     const [month, day, year] = value.split('/');
    
    //     const isoFormattedStr = `${year}-${month}-${day}`;
    
    //     const date = new Date(isoFormattedStr);
    
    //     const timestamp = date.getTime();
    
    //     if (typeof timestamp !== 'number' || Number.isNaN(timestamp)) {
    //     return false;
    //     }
    
    //     return date.toISOString().startsWith(isoFormattedStr);
    // }
};

