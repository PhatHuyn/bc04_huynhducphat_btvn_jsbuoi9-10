function NhanVien(taikhoannv, hotennv, emailnv, matkhaunv,  ngaylamnv, luongcobannv, chucvunv, giolamnv){
    this.TaiKhoanNV = taikhoannv;
    this.HoTenNV = hotennv;
    this.EmailNV = emailnv;
    this.MatKhauNV = matkhaunv;
    this.NgayLamNV = ngaylamnv;
    this.LuongCoBanNV = luongcobannv;
    this.ChucVuNV = chucvunv;
    this.GioLamNV = giolamnv;
    this.TinhTongLuong = function(){
        var sum = 0;
        if(this.ChucVuNV == "Sếp"){
            sum =  (this.LuongCoBanNV * 1) * 3;
        }else if(this.ChucVuNV == "Trưởng phòng"){
            sum =  (this.LuongCoBanNV * 1) * 2;
        }else{
            sum =  (this.LuongCoBanNV * 1);
        }
        return sum;
    };
    this.XepLoaiNV = function(){
        var XepLoai = "";
        if(this.GioLamNV >= 192){
            XepLoai = "Nhân viên xuất sắc";
        }else if(this.GioLamNV >= 176){
            XepLoai = "Nhân viên giỏi";
        }else if(this.GioLamNV >= 160){
            XepLoai = "Nhân viên khá";
        }else{
            XepLoai = "Nhân viên trung bình";
        }
        return XepLoai;
    }
}