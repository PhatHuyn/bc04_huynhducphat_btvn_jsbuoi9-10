const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

//chức năng thêm SV
var dsnv = [];

// lấy thông tin từ LocalStorage
var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if(dsnvJson != null){
    dsnv = JSON.parse(dsnvJson);

    for(var i = 0; i<dsnv.length; i++){
        var nv = dsnv[i];
        dsnv[i] = new NhanVien(
            nv.TaiKhoanNV,
            nv.HoTenNV,
            nv.EmailNV,
            nv.MatKhauNV,
            nv.NgayLamNV,
            nv.LuongCoBanNV,
            nv.ChucVuNV,
            nv.GioLamNV
        );
    }
    renderDSNV(dsnv);
}

function ThemNguoiDung(){
    var newNV = LayThongTintuForm();
    //kiểm tra validate
    var isValid = validate.kiemTraRong(newNV.TaiKhoanNV, "tbTKNV", "Tài khoản nhân viên không được để trống") &&
    validate.kiemTraDoDai(newNV.TaiKhoanNV, "tbTKNV", "Tài khoản nhân viên từ 4 đến 6 ký tự", 4, 6);

    isValid = isValid &
    validate.kiemTraRong(newNV.HoTenNV, "tbTen", "Tên nhân viên không được trống") &&
    validate.kiemtraChu(newNV.HoTenNV, "tbTen", "Tên nhân viên phải là chữ");

    isValid = isValid &
    validate.kiemTraRong(newNV.EmailNV, "tbEmail", "Email nhân viên không được trống") &&
    validate.kiemtraEmail(newNV.EmailNV, "tbEmail", "Email không hợp lệ");

    isValid = isValid &
    validate.kiemTraRong(newNV.MatKhauNV, "tbMatKhau", "Mật khẩu nhân viên không được trống");

    isValid = isValid &
    validate.kiemTraRong(newNV.NgayLamNV, "tbNgay", "Ngày làm không được trống");

    isValid = isValid &
    validate.kiemTraRong(newNV.LuongCoBanNV, "tbLuongCB", "Lương nhân viên không được trống") &&
    validate.kiemtraSo(newNV.LuongCoBanNV, "tbLuongCB", "Lương nhân viên phải nhập số, không nhập khoảng cách hay chấm phẩy") &&
    validate.kiemtraLuong(newNV.LuongCoBanNV, "tbLuongCB", "Lương nhân viên từ 1.000.000 - 20.000.000");

    isValid = isValid &
    validate.kiemTraRong(newNV.ChucVuNV, "tbChucVu", "Chức vụ phải chọn hợp lệ");

    isValid = isValid &
    validate.kiemTraRong(newNV.GioLamNV, "tbGiolam", "Giờ làm không được trống") &&
    validate.kiemtraGio(newNV.GioLamNV, "tbGiolam", "Giờ làm không hợp lệ. Vui lòng nhập từ 80 - 200.");

    if(isValid){
    dsnv.push(newNV);
    console.log('dsnv: ', dsnv);
    // tạo Json
    var dsnvJson = JSON.stringify(dsnv);
    // lưu json vào local Strorage
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
    }
}

function xoaNhanVien(id){
    var index = TimKiemViTri(id, dsnv);
    if(index != -1){
        dsnv.splice(index, 1);
        // tạo Json
        var dsnvJson = JSON.stringify(dsnv);
        // lưu json vào local Strorage
        localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
        renderDSNV(dsnv);
    }
}

function suaNhanVien(id){
    var index = TimKiemViTri(id, dsnv);

    if(index != -1){
        var nv = dsnv[index];
        ShowThongTinLenForm(nv);
    }
}

function CapNhatNV(){
    var UpdateNV = LayThongTintuForm2();
    //kiểm tra validate
    var isValid = validate.kiemTraRong(UpdateNV.HoTenNV, "tbTen2", "Tên nhân viên không được trống") &&
    validate.kiemtraChu(UpdateNV.HoTenNV, "tbTen2", "Tên nhân viên phải là chữ");

    isValid = isValid &
    validate.kiemTraRong(UpdateNV.EmailNV, "tbEmail2", "Email nhân viên không được trống") &&
    validate.kiemtraEmail(UpdateNV.EmailNV, "tbEmail2", "Email không hợp lệ");

    isValid = isValid &
    validate.kiemTraRong(UpdateNV.MatKhauNV, "tbMatKhau2", "Mật khẩu nhân viên không được trống");

    isValid = isValid &
    validate.kiemTraRong(UpdateNV.NgayLamNV, "tbNgay2", "Ngày làm không được trống");

    isValid = isValid &
    validate.kiemTraRong(UpdateNV.LuongCoBanNV, "tbLuongCB2", "Lương nhân viên không được trống") &&
    validate.kiemtraSo(UpdateNV.LuongCoBanNV, "tbLuongCB2", "Lương nhân viên phải nhập số, không nhập khoảng cách hay chấm phẩy") &&
    validate.kiemtraLuong(UpdateNV.LuongCoBanNV, "tbLuongCB2", "Lương nhân viên từ 1.000.000 - 20.000.000");

    isValid = isValid &
    validate.kiemTraRong(UpdateNV.ChucVuNV, "tbChucVu2", "Chức vụ phải chọn hợp lệ");

    isValid = isValid &
    validate.kiemTraRong(UpdateNV.GioLamNV, "tbGiolam2", "Giờ làm không được trống") &&
    validate.kiemtraGio(UpdateNV.GioLamNV, "tbGiolam2", "Giờ làm không hợp lệ. Vui lòng nhập từ 80 - 200.");

    if(isValid){
    CapNhatnv(UpdateNV, dsnv);
    // tạo Json
    var dsnvJson = JSON.stringify(dsnv);
    // lưu json vào local Strorage
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
    }
}

function TimkiemLoai(){
    let valueSearchInput = document.getElementById("searchName").value;
    let kq = [];
    if(valueSearchInput == ""){
        document.getElementById("thongbaoloisearch").style.display = "block";
        document.getElementById("thongbaoloisearch").innerHTML = `Chưa nhập thông tin tìm kiếm.`
    }else{
        document.getElementById("thongbaoloisearch").style.display = "none";
        for(let i = 0;i<dsnv.length;i++){
            var nvs = dsnv[i].XepLoaiNV();
            var nv = dsnv[i];

            console.log('nvs: ', nvs);
            if(nvs.includes(valueSearchInput)){
                console.log('danh sách: ', nv);
                kq.push(nv);
                renderDSNV(kq);
                console.log("Ok");
            }else{
                console.log("hỏng rồi!!!");
            }
        }
    }
}
